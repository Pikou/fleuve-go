package fr.immortal.diablo.fleuvego;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Pi extends AppCompatActivity {

    Pi pi = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pi);

        final PointInteret pointInteret = (PointInteret) getIntent().getExtras().get("Pi");
        TextView textViewPI = findViewById(R.id.all);

        textViewPI.setText(pointInteret.toString());

        Button buttonAddFav = (Button) findViewById(R.id.button);
        buttonAddFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FavJson.addJson(pointInteret, getFilesDir());

                Toast.makeText(pi, "Ajouté aux Favoris", Toast.LENGTH_LONG).show();
            }
        });
    }
}
