package fr.immortal.diablo.fleuvego;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Arrays;

public class PointInteret implements Serializable {
    private String commune;
    private String ensem2;
    private int code_carto;
    private int identifian;
    private float st_lengths;
    private float st_areasha;
    private String histo1;
    private float[] geo_point_2d;
    private String elem_princ;
    private String elem_patri;

    @Override
    public String toString() {
        return "PointInteret{" +
                "commune='" + commune + '\'' +
                ", ensem2='" + ensem2 + '\'' +
                ", code_carto=" + code_carto +
                ", identifian=" + identifian +
                ", st_lengths=" + st_lengths +
                ", st_areasha=" + st_areasha +
                ", histo1='" + histo1 + '\'' +
                ", geo_point_2d=" + Arrays.toString(geo_point_2d) +
                ", elem_princ='" + elem_princ + '\'' +
                ", elem_patri='" + elem_patri + '\'' +
                ", record_timestamp='" + record_timestamp + '\'' +
                '}';
    }

    public int getIdentifian() {
        return identifian;
    }

    public void setIdentifian(int identifian) {
        this.identifian = identifian;
    }

    private String record_timestamp;

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getEnsem2() {
        return ensem2;
    }

    public void setEnsem2(String ensem2) {
        this.ensem2 = ensem2;
    }

    public int getCode_carto() {
        return code_carto;
    }

    public void setCode_carto(int code_carto) {
        this.code_carto = code_carto;
    }

    public float getSt_lengths() {
        return st_lengths;
    }

    public void setSt_lengths(float st_lengths) {
        this.st_lengths = st_lengths;
    }

    public float getSt_areasha() {
        return st_areasha;
    }

    public void setSt_areasha(float st_areasha) {
        this.st_areasha = st_areasha;
    }

    public String getHisto1() {
        return histo1;
    }

    public void setHisto1(String histo1) {
        this.histo1 = histo1;
    }

    public float[] getGeo_point_2d() {
        return geo_point_2d;
    }

    public void setGeo_point_2d(float[] geo_point_2d) {
        this.geo_point_2d = geo_point_2d;
    }

    public String getElem_princ() {
        return elem_princ;
    }

    public void setElem_princ(String elem_princ) {
        this.elem_princ = elem_princ;
    }

    public String getElem_patri() {
        return elem_patri;
    }

    public void setElem_patri(String elem_patri) {
        this.elem_patri = elem_patri;
    }

    public String getRecord_timestamp() {
        return record_timestamp;
    }

    public void setRecord_timestamp(String record_timestamp) {
        this.record_timestamp = record_timestamp;
    }
}
