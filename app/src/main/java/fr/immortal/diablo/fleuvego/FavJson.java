package fr.immortal.diablo.fleuvego;

import android.os.StrictMode;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class FavJson {

    private static ArrayList<PointInteret> pointInteretArrayList = new ArrayList<>();
    private static String filename = "favoris.json";

    public static void addJson(PointInteret pointInteret, File dir) {
        pointInteretArrayList.clear();
        final Gson gson = new GsonBuilder().create();
        InputStream inputStream = null;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        File file = new File(dir, filename);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                inputStream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            final JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream));
            final JsonParser jsonParser = new JsonParser();
            final JsonArray jsonArray = jsonParser.parse(jsonReader).getAsJsonArray();

            for (final JsonElement element : jsonArray) {
                final PointInteret pointInteretInFor = gson.fromJson(element.toString(), PointInteret.class);
                pointInteretArrayList.add(pointInteretInFor);
            }
        }

        pointInteretArrayList.add(pointInteret);
        String outpoutString = gson.toJson(pointInteretArrayList);

        FileOutputStream outputStream = null;
        try {
            policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            outputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            outputStream.write(outpoutString.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<PointInteret> getArrayFav(File dir) {
        pointInteretArrayList.clear();
        final Gson gson = new GsonBuilder().create();
        InputStream inputStream = null;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        File file = new File(dir, filename);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                inputStream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            final JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream));
            final JsonParser jsonParser = new JsonParser();
            final JsonArray jsonArray = jsonParser.parse(jsonReader).getAsJsonArray();

            for (final JsonElement element : jsonArray) {
                final PointInteret pointInteretInFor = gson.fromJson(element.toString(), PointInteret.class);
                pointInteretArrayList.add(pointInteretInFor);
            }
        }

        return pointInteretArrayList;
    }
}
