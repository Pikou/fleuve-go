package fr.immortal.diablo.fleuvego;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class PointInteretList extends AppCompatActivity {

    PointInteretList pointInteretList = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_interet_list);

        final List<PointInteret> listPI = (List<PointInteret>) getIntent().getSerializableExtra("listPI");

        ListView listViewPI = (ListView) findViewById(R.id.listViewPI);
        PiAdapter piAdapter = new PiAdapter(PointInteretList.this, listPI);

        listViewPI.setAdapter(piAdapter);
        listViewPI.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(pointInteretList, Pi.class);

                PointInteret pointInteret = listPI.get(i);
                intent.putExtra("Pi", pointInteret);
                pointInteretList.startActivityForResult(intent, -1);
            }
        });
    }
}
