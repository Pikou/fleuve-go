package fr.immortal.diablo.fleuvego;

import android.content.Context;
import android.os.StrictMode;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


public class TraitementJson {

    private static LinkedHashMap<String, List<PointInteret>> pIbyCommune = new LinkedHashMap<>();
    private static String filename = "patrimoine-fluvial.small.json";

    public static LinkedHashMap<String, List<PointInteret>> traitementBigJSON(MainActivity mainActivity){
        pIbyCommune.clear();
        final Gson gson = new GsonBuilder().create();

        try {
            File file = new File(mainActivity.getFilesDir(), filename);

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL url = new URL("https://www.data.gouv.fr/fr/datasets/r/cb99a6b9-7e59-4ea4-9d58-a1d89a7a39a6");
            URLConnection urlConnection = url.openConnection();
            urlConnection.setConnectTimeout(1000);
            InputStream json = urlConnection.getInputStream();

            final List<PointInteret> interets = new ArrayList<>();
            final JsonReader jsonReader = new JsonReader(new InputStreamReader(json));
            final JsonParser jsonParser = new JsonParser();
            final JsonArray jsonArray = jsonParser.parse(jsonReader).getAsJsonArray();

            for (final JsonElement element : jsonArray) {
                JsonObject jsonObject = element.getAsJsonObject();
                JsonElement jsonElement = jsonObject.get("fields");

                final PointInteret pointInteret = gson.fromJson(jsonElement.toString(), PointInteret.class);
                addPIinCommune(jsonObject.get("fields").getAsJsonObject().get("commune").getAsString(), pointInteret);

                interets.add(pointInteret);
            }
            jsonReader.close();
            String jsonSmall = gson.toJson(interets);

            try {
                FileOutputStream outputStream;
                outputStream = mainActivity.openFileOutput(filename, Context.MODE_PRIVATE);
                outputStream.write(jsonSmall.getBytes());
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }

        return pIbyCommune;
    }

    public static LinkedHashMap<String, List<PointInteret>> traitementSmallJSON(MainActivity mainActivity){
        pIbyCommune.clear();
        final Gson gson = new GsonBuilder().create();

        try {
            File file = new File(mainActivity.getFilesDir(), filename);

            InputStream json = mainActivity.openFileInput(filename);

            final JsonReader jsonReader = new JsonReader(new InputStreamReader(json));
            final JsonParser jsonParser = new JsonParser();
            final JsonArray jsonArray = jsonParser.parse(jsonReader).getAsJsonArray();

            for (final JsonElement element : jsonArray) {
                JsonObject object = element.getAsJsonObject();

                final PointInteret pointInteret = gson.fromJson(object, PointInteret.class);
                addPIinCommune(object.get("commune").getAsString(), pointInteret);
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }

        return pIbyCommune;
    }


    private static void addPIinCommune(String commune, PointInteret pointInteret) {
        if (pIbyCommune.containsKey(commune)) {
            List<PointInteret> pointInterets = pIbyCommune.get(commune);
            pointInterets.add(pointInteret);

            pIbyCommune.put(commune, pointInterets);
        }

        else {
            List<PointInteret> pointInterets = new ArrayList<>();
            pointInterets.add(pointInteret);

            pIbyCommune.put(commune, pointInterets);
        }
    }
}

