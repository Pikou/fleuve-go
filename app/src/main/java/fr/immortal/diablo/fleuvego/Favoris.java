package fr.immortal.diablo.fleuvego;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Favoris extends AppCompatActivity {

    Favoris favoris = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoris);

        final List<PointInteret> pointInteretList = FavJson.getArrayFav(getFilesDir());

        ListView listViewPI = (ListView) findViewById(R.id.listFav);
        PiAdapter piAdapter = new PiAdapter(this, pointInteretList);

        listViewPI.setAdapter(piAdapter);
        listViewPI.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(favoris, Pi.class);

                PointInteret pointInteret = pointInteretList.get(i);
                intent.putExtra("Pi", pointInteret);
                favoris.startActivityForResult(intent, -1);
            }
        });
    }
}
