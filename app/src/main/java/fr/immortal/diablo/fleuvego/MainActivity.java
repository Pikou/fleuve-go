package fr.immortal.diablo.fleuvego;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private ListView lvCommune;

    private MainActivity mainActivity = this;
    private LinkedHashMap<String, List<PointInteret>> listByCommune;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (new File(mainActivity.getFilesDir(), "patrimoine-fluvial.small.json").exists()){
            Toast.makeText(this, "Traitement du JSON small", Toast.LENGTH_LONG).show();
            listByCommune = TraitementJson.traitementSmallJSON(mainActivity);

            Toast.makeText(this, "Done !", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Traitement du JSON big", Toast.LENGTH_LONG).show();
            listByCommune = TraitementJson.traitementBigJSON(mainActivity);

            Toast.makeText(this, "Done !", Toast.LENGTH_LONG).show();
        }

        LinkedHashMapAdapter<String, List<PointInteret>> hashMapAdapter =
                new LinkedHashMapAdapter<String, List<PointInteret>>(this, R.layout.commune, listByCommune);

        final ArrayList<String> citys = new ArrayList<String>();

        for ( String key : listByCommune.keySet() ) {
            citys.add(key);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        lvCommune = (ListView)findViewById(R.id.lvCommune);
        lvCommune.setAdapter(hashMapAdapter);
        lvCommune.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String city = citys.get(i);
                intent = new Intent(mainActivity, PointInteretList.class);

                List<PointInteret> pointInteretList = listByCommune.get(city);
                ArrayList<PointInteret> pointInteretArrayList = new ArrayList<PointInteret>();
                for(int ii=0; ii < pointInteretList.size(); ii++){
                    pointInteretArrayList.add(pointInteretList.get(ii));
                }

                intent.putExtra("listPI", pointInteretArrayList);
                mainActivity.startActivityForResult(intent, -1);
            }
        });
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Mise à jour des données à partir d'Internet", Snackbar.LENGTH_SHORT).show();

                listByCommune = TraitementJson.traitementBigJSON(mainActivity);

                Toast.makeText(mainActivity, "Done ! Restart Activity", Toast.LENGTH_SHORT).show();

                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {
            Intent intentFav = new Intent(mainActivity, Favoris.class);
            startActivityForResult(intentFav, -1);
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
