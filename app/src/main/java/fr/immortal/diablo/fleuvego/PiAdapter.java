package fr.immortal.diablo.fleuvego;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class PiAdapter extends ArrayAdapter<PointInteret> {
    public PiAdapter(Context context, List<PointInteret> pointInteretList) {
        super(context, 0, pointInteretList);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.un_point_interet_list, viewGroup, false);
        }

        PiViewHolder viewHolder = (PiViewHolder) view.getTag();
        if(viewHolder == null){
            viewHolder = new PiViewHolder();
            viewHolder.elem_patri = (TextView) view.findViewById(R.id.elem_patri);
            viewHolder.ensem2 = (TextView) view.findViewById(R.id.ensem2);
            viewHolder.elem_princ = (TextView) view.findViewById(R.id.elem_princ);
            view.setTag(viewHolder);
        }

        PointInteret pointInteret = (PointInteret) getItem(i);
        viewHolder.elem_patri.setText(pointInteret.getElem_patri());
        viewHolder.ensem2.setText(pointInteret.getEnsem2());
        viewHolder.elem_princ.setText(pointInteret.getElem_princ());

        return view;
    }
}

class PiViewHolder {
    public TextView elem_patri;
    public TextView ensem2;
    public TextView elem_princ;
}